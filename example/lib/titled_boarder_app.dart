import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';

class TitledBoarderApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Shaped Image Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: TitledBoarderHomePage());
  }
}

class TitledBoarderHomePage extends StatelessWidget {
  const TitledBoarderHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
            /* Our library use! */
            child: SubmitForm()),
      ),
    );
  }
}

class SubmitForm extends StatefulWidget {
  SubmitForm({Key key}) : super(key: key);

  @override
  _SubmitFormState createState() => _SubmitFormState();
}

class _SubmitFormState extends State<SubmitForm> {
  @override
  Widget build(BuildContext context) {
    final userNameController = TextEditingController();
    final _formKey = GlobalKey<FormState>();

    return new Form(
      key: _formKey,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(height: 10),
            TitledBoarderFormField(
              controller: userNameController,
              icon: Icons.account_circle,
              title: "user name",
            ),
            RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                ),
                child: Text("Save",
                    style: TextStyle(
                        color: Color.fromRGBO(0, 0, 255, 1), fontSize: 20)),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    //string username = userNameController.text;
                  }
                })
          ],
        ),
      ),
    );
  }
}
