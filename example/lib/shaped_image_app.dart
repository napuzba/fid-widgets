import 'package:fid_widgets/fid_widgets.dart';
import 'package:flutter/material.dart';

class ShapedImageApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Shaped Image Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: ShapedImageHomePage());
  }
}

class ShapedImageHomePage extends StatelessWidget {
  const ShapedImageHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          /* Our library use! */
          child: ShapedImage(
            imagePath:
                "https://cdn.vox-cdn.com/thumbor/Iy012aBHCHAESjEyc2KZWhweHaI=/0x0:4635x3090/1200x800/filters:focal(1928x400:2668x1140)/cdn.vox-cdn.com/uploads/chorus_image/image/66114445/1199341672.jpg.0.jpg",
            imageSource: ImageSourceType.Network,
            shape: ImageShapeType.Circle,
            raduis: 100,
          ),
        ),
      ),
    );
  }
}
