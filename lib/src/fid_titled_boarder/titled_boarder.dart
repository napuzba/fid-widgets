import 'package:flutter/material.dart';

class TitledBoarder extends StatelessWidget {
  final Text title;
  final Widget child;
  final Color color;

  const TitledBoarder(
      {Key key, @required this.title, @required this.child, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            height: 70,
            child: Padding(
              padding: EdgeInsets.only(top: 5),
              child: Container(
                padding: EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: this.color == null ? Color(0x22383838) : this.color,
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(5.0) //         <--- border radius here
                      ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: child,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 50),
            child: Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: title,
            ),
          ),
        ],
      ),
    );
  }
}

class TitledBoarderFormField extends StatefulWidget {
  final IconData icon;
  final String title;
  final FormFieldValidator<String> validator;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final Color textForgroundColor;

  TitledBoarderFormField(
      {Key key,
      @required this.icon,
      @required this.title,
      this.controller,
      this.validator,
      this.keyboardType, this.textForgroundColor = Colors.black87})
      : super(key: key);

  @override
  _TitledBoarderFormFieldState createState() => _TitledBoarderFormFieldState();
}

class _TitledBoarderFormFieldState extends State<TitledBoarderFormField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TitledBoarder(
          color: widget.textForgroundColor,
          title: Text(
            widget.title,
            style: TextStyle(color: widget.textForgroundColor),
          ),
          child: Row(
            children: <Widget>[
              Icon(
                widget.icon,
                color: widget.textForgroundColor,
              ),
              SizedBox(
                width: 20,
              ),
              new Flexible(
                /* if you put widgets that don't have an intrinsic width inside a row, you must nest them inside a flexible widget. */
                child: TextFormField(
                    controller: widget.controller,
                    keyboardType: widget.keyboardType,
                    cursorColor: widget.textForgroundColor,
                    style:
                        TextStyle(color: widget.textForgroundColor),
                    // The validator receives the text that the user has entered.
                    validator: widget.validator),
              )
            ],
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          "    Enter " + widget.title.toLowerCase(),
          style: TextStyle(
              fontSize: 12, color: widget.textForgroundColor),
        ),
      ],
    );
  }
}
