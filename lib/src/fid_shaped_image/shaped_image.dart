import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

enum ImageShapeType { Circle, Squer }
enum ImageSourceType { Network, Local }

class ShapedImage extends StatelessWidget {
  final ImageShapeType shape;
  final String imagePath;
  final ImageSourceType imageSource;
  final double raduis;

  const ShapedImage({
    Key key,
    @required this.imagePath,
    @required this.imageSource,
    this.shape = ImageShapeType.Circle,
    this.raduis = 100,
  }) : super(key: key);

  ImageProvider<dynamic> _fetchImage() {
    return imageSource == ImageSourceType.Network
        ? CachedNetworkImageProvider(
            this.imagePath,
          )
        : ExactAssetImage(this.imagePath);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      child: shape == ImageShapeType.Circle
          ? CircleAvatar(
              radius: this.raduis,
              backgroundImage: _fetchImage(),
            )
          : Image(image: _fetchImage()),
    );
  }
}
