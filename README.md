# fid_widgets

Main library of FID - Flutter Israel Developers team.
This library includes all of the other libraries that will be
developed by team members.

## How to Contribute to Exists/New Library? 
1. Clone this project.
2. Checkout to new branch with naming convention of: `dev__<your-name>_fid-<library-name>`.
3. Develop your library under `lib/src/fid-<lib-name>/<lib-dart-file>.dart`.
    You can run the app, by adding using your library in the main function at: `example/lib/main.dart`.
4. Add it to `lib/fid_widgets.dart` as: `export 'src/fid-<lib-name>/<lib-dart-file>.dart';`
5. Fix the version number in `./pubspec.yaml` file.
6. Open `CHANGELOG`, and write your changes there.
7. Open terminal and run:
```
> flutter pub publish --dry-run
Package has 0 warnings.
```
8. If in 4 the output show 0 errors/warnings, run the following(and authenticated if needed):
```
> flutter packages pub publish
...
Successfully uploaded package.
```

9. See the changes on [pub.dev](https://pub.dev/packages/fid_widgets)

